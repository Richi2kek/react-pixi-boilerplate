import * as React from 'react';
import {
    Switch,
    Route,
} from 'react-router-dom';

import DashboardPage from '../pages/dashboard/DashboardPage';
import NotFoundPage from '../pages/notFound/NotFoundPage';

interface Props {
}

interface State {
}

export default class AppRouter extends React.Component<Props, State> {

    constructor(props: Props, state: State) {
        super(props);
        this.state = {
            open: false
        };
    }
    render() {
        return (
            <div className="app-router">
                <Switch>
                    <Route exact={true} path="/" component={DashboardPage} />
                    <Route path="/dashboard" component={DashboardPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </div>
        );
    }
}
