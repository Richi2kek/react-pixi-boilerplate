import * as React from 'react';
import AppRouter from './AppRouter';

// import DefaultLayout from '../components/layouts/default-layout/DefaultLayout';
// import GameLayout from '../components/layouts/game-layout/GameLayout';
import { BurgerLayout } from '../components/layouts/burger-layout/BurgerLayout';

import config from '../config/app.config';

interface Props {
  
}

import { IDefaultLink } from '../components/links/model/IDefaultLink';

interface State {
  title: string;
  links: IDefaultLink[];
}
import './App.css';

class App extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      title: config.app.title,
      links: [
        {name: 'home', href: '', },
        {name: 'dashboard', href: ''}
      ]
    };
  }

  render() {
    return (
      <div className="app">
        <BurgerLayout
          animation="scaleRotate"
          links={this.state.links}
        >
          <AppRouter />
        </BurgerLayout>
      </div>
    );
  }
}

export default App;
