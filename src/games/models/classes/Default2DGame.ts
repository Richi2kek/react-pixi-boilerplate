import TwoDimensionsGame from '../interfaces/TwoDimensionsGame.interface';

/**
 * TODO:
 * # Add helpers:
 * - keyboard
 * - hitTestRectangle
 * - contain
 * - randomInt
 */

export default abstract class Default2DGame implements TwoDimensionsGame {
    app: any;
    state: any;
    start(app: any) {
        console.log('game setup');
        this.app = app;
    }

    setup(loader: any, resources: any) {
        console.log('game start');
        this.state = this.play;
        this.app.ticker.add((delta: any) => this.gameLoop(delta));
    }

    play(delta: any) {
        console.log('game play');
    }

    gameLoop(delta: any)  {
        console.log('game loop');
    }

    end() {
        console.log('game end');
    }

}