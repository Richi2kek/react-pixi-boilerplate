export default interface TwoDimensionsGame {
    state: any;

    start(app: any): void;
    setup(loader: any, resources: any): void;
    play(delta: any): void;
    gameLoop(delta: any): void;
    end(): void;
}