
import * as Pixi from 'pixi.js';
import Default2DGame from '../models/classes/Default2DGame';

export default class TreasureHunterGame extends Default2DGame {
    state: any;
    app: any;
    scenes: Pixi.Container[] = [];
    sprites: Pixi.Sprite[] = [];

    start(app: any): void {
        this.app = app;
        Pixi.loader
            .add('treasureHunter', '/assets/treasureHunter.json')
            .load((loader: any, resources: any) => {
                this.setup(loader, resources);
            });
    }

    setup(loader: any, resources: any): void {
        // get textures refs
        const texturesIds = resources.treasureHunter.textures;
        // load the games
        this.load(texturesIds);
        // gogogogoogoooo
        this.state = this.play; // set game state
        this.app.ticker.add((delta: any) => { this.gameLoop(delta); });
    }

    gameLoop(delta: any) {
        // game loop
        this.play(delta);
    }

    play(delta: any) {
        // all game logic

    }

    end(): void {
        console.log('Game end!');
    }

    private load(texturesIds: any) {
        // Create the `gameScene` group
        const gameScene = new Pixi.Container();
        const gameSceneKey = 'game';
        this.scenes[gameSceneKey] = gameScene;
        this.app.stage.addChild(gameScene);

        const gameOverScene = new Pixi.Container();
        const gameOverSceneKey = 'gameOver';
        this.scenes[gameOverSceneKey] = gameOverScene;
        this.app.stage.addChild(gameOverScene);

        gameOverScene.visible = false; // !!/!\!!

        // Create the health bar bof bof
        // Add some text for the game over message

        // Create a `gameOverScene` group
        // Assign the player's keyboard controllers

        // dungeon sprite from json sprite atlas
        const dungeonTexture = texturesIds['dungeon.png'];
        const dungeonSprite = new Pixi.Sprite(dungeonTexture);
        const dungeonSpriteKey = 'dungeon';
        dungeonSprite.x = 0;
        dungeonSprite.y = 0;
        this.sprites[dungeonSpriteKey] = dungeonSprite;
        gameScene.addChild(dungeonSprite);

        // dungeon sprite from json sprite atlas
        const doorTexture = texturesIds['door.png'];
        const doorSprite = new Pixi.Sprite(doorTexture);
        const doorSpriteKey = 'door';
        doorSprite.x = 32;
        doorSprite.y = 0;
        this.sprites[doorSpriteKey] = doorSprite;
        gameScene.addChild(doorSprite);
        // treasure sprite from json sprite atlas
        const treasureTexture = texturesIds['treasure.png'];
        const treasureSprite = new Pixi.Sprite(treasureTexture);
        const treasureSpriteKey = 'treasure';
        treasureSprite.x = 100;
        treasureSprite.y = 100;
        this.sprites[treasureSpriteKey] = treasureSprite;
        gameScene.addChild(treasureSprite);

        // explorer sprite from json sprite atlas
        const explorerTexture = texturesIds['explorer.png'];
        const explorerSprite = new Pixi.Sprite(explorerTexture);
        const explorerSpriteKey = 'explorer';
        explorerSprite.x = 200;
        explorerSprite.y = 200;
        // explorerSprite.
        this.sprites[explorerSpriteKey] = explorerSprite;
        gameScene.addChild(explorerSprite);

        // blob sprite from json sprite atlas
        const blobTexture = texturesIds['blob.png'];
        const blobs = [];
        let numberOfBlobs = 6,
            spacing = 75,
            xOffset = 15;
        for (let i = 0; i < numberOfBlobs; i++) {
            let blob = new Pixi.Sprite(blobTexture);

            let x = spacing * i + xOffset;
            let y = Math.floor(Math.random() * (this.app.stage.height - blob.height));

            blob.x = x;
            blob.y = y;

            blobs.push(blob);
            gameScene.addChild(blob);
        }

        const blobSpriteKey = 'blobs';
        this.sprites[blobSpriteKey] = blobs;

        // Making the health bar
        const healthBar = new Pixi.Container();
        healthBar.position.set(this.app.stage.width - 170, 4);
        gameScene.addChild(healthBar);
        // create black background for the bar
        const innerBar = new Pixi.Graphics();
        innerBar.beginFill(0x0000000);
        innerBar.endFill();
        healthBar.addChild(healthBar);

        // create the front red rectangle
        const outerBar = new PIXI.Graphics();
        outerBar.beginFill(0xFF3300);
        outerBar.drawRect(0, 0, 128, 8);
        outerBar.endFill();
        healthBar.addChild(outerBar);

        // create the message text
        const style = new Pixi.TextStyle({
            fontFamily: 'Futura',
            fontSize: 64,
            fill: 'white'
        });
        let message = new Pixi.Text('The End!', style);
        message.x = 120;
        message.y = this.app.stage.height / 2 - 32;
        gameOverScene.addChild(message);

    }
}
