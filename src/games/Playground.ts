
import * as Pixi from 'pixi.js';
import Default2DGame from '../games/models/classes/Default2DGame';

export default class PlaygroundGame extends Default2DGame {
    app: any;

    start(app: any): void {
        console.log('setup playground game');
        this.app = app;
        Pixi.loader
            .add('cat', '/assets/cat.png')
            .add('tileset', '/assets/tileset.png')
            .add('treasureHunter', '/assets/treasureHunter.json')
            .load((loader: any, resources: any) => {
                this.setup(loader, resources);
            });
    }

    setup(loader: any, resources: any): void {
        console.log(loader, resources);
        console.log('start playground game');
        // blob sprite from json sprite atlas
        const dungeonResource = resources.treasureHunter.textures['dungeon.png'].textureCacheIds;
        const dungeonTexture = Pixi.Texture.fromFrame(dungeonResource);
        const dungeonSprite = new Pixi.Sprite(dungeonTexture);
        dungeonSprite.x = 0;
        dungeonSprite.y = 0;

        this.app.stage.addChild(dungeonSprite);
        // cat sprite
        const catTexture = Pixi.Texture.fromFrame(resources.cat.url);
        const catSprite = new Pixi.Sprite(catTexture);
        const catPosX = 250;
        const catPosY = 250;
        catSprite.anchor.x = 0.5;
        catSprite.anchor.y = 0.5;
        catSprite.position.x = catPosX;
        catSprite.position.y = catPosY;
        catSprite.scale.x = 2;
        catSprite.scale.y = 2;
        this.app.stage.addChild(catSprite);
        // rocket sprite
        const rocketTexture = Pixi.Texture.fromImage(resources.tileset.url);
        const rocketRectangle = new Pixi.Rectangle(192, 128, 64, 64);
        rocketTexture.frame = rocketRectangle;
        const rocketSprite = new Pixi.Sprite(rocketTexture);
        rocketSprite.x = 300;
        rocketSprite.y = 300;
        rocketSprite.scale.x = 0.5;
        rocketSprite.scale.y = 0.5;
        this.app.stage.addChild(rocketSprite);
        // blob sprite from json sprite atlas
        const blobResource = resources.treasureHunter.textures['blob.png'].textureCacheIds;
        const blobTexture = Pixi.Texture.fromFrame(blobResource);
        const blobSprite = new Pixi.Sprite(blobTexture);
        blobSprite.anchor.x = 0.5;
        blobSprite.anchor.y = 0.5;
        blobSprite.x = 150;
        blobSprite.y = 150;
        this.app.stage.addChild(blobSprite);
        // treasure sprite from json sprite atlas
        const treasureResource = resources.treasureHunter.textures['treasure.png'].textureCacheIds;
        const treasureTexture = Pixi.Texture.fromFrame(treasureResource);
        const treasureSprite = new Pixi.Sprite(treasureTexture);
        treasureSprite.anchor.x = 0.5;
        treasureSprite.anchor.y = 0.5;

        treasureSprite.x = 100;
        treasureSprite.y = 100;
        this.app.stage.addChild(treasureSprite);
        // explorer sprite from json sprite atlas
        const explorerResource = resources.treasureHunter.textures['explorer.png'].textureCacheIds;
        const explorerTexture = Pixi.Texture.fromFrame(explorerResource);
        const explorerSprite = new Pixi.Sprite(explorerTexture);
        explorerSprite.x = 50;
        explorerSprite.y = 50;
        this.app.stage.addChild(explorerSprite);

        this.app.ticker.add((delta: any) => { this.gameLoop(delta); });

    }

    gameLoop(delta: any) {
        // some check
        this.play(delta);
    }

    play(delta: any) {
        // all game logic
        console.log('play');
        // catSprite.rotation += 0.05 * delta;
        // rocketSprite.rotation -= 0.05 * delta;
        // blobSprite.rotation += 0.05;
        // treasureSprite.rotation -= 0.05;
    }

    end(): void {
        console.log('Game end!');
    }
}
