import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Main from './Main';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

import 'typeface-roboto';
import 'antd/dist/antd.less';

ReactDOM.render(
  <Main />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
