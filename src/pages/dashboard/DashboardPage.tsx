import * as React from 'react';

import TreasureHunterGame from '../../games//treasure-hunter/TreasureHunter';
import VerticalSplitPane from '../../components/split-panes/vertical-split-pane/VerticalSplitPane';
import DefaultCanvas from '../../components/canvas/default-canvas/DefaultCanvas';
import DefaultCodeEditor from '../../components/code-editor/default-code-editor/DefaultCodeEditor';

import './DashboardPage.css';

interface Props {

}

interface State {
}

export default class DashboardPage extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div className="dashboard-page">
                <div className="dashboard-window">
                    <VerticalSplitPane
                        left={
                            <DefaultCodeEditor />
                        }
                        right={
                            <DefaultCanvas
                                game={
                                    new TreasureHunterGame()
                                }
                            />
                        }
                    />
                </div>
            </div>
        );
    }
}
