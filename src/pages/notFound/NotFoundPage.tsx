import * as React from 'react';

import DefaultCanvas from '../../components/canvas/default-canvas/DefaultCanvas';

import './NotFoundPage.css';

interface Props {

}

interface State {
}

export default class DashboardPage extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);
    }
    render() {
        return (
            <div className="not-found-page">
                <DefaultCanvas />
            </div>
        );
    }
}
