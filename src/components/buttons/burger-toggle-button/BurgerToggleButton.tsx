import * as React from 'react';

import { Props } from './Props';
import { State } from './State';
import './BurgerToggleButton.css';

export class BurgerToggleButton extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        const message = this.props.isOpen ? 'close' : 'open';
        return (
            <div>
                <button
                    onClick={this.props.onClick}
                >
                    {message}
                </button>
            </div>
        );
    }
}
