/**
 * External dependencies
 */
import * as React from 'react';
import * as CodeMirror from 'react-codemirror';

import 'codemirror/mode/javascript/javascript';
/**
 * Locals dependencies
 */
import { Props } from './Props'; // the props of the component
import { State } from './State'; // the state of the component
import './DefaultCodeEditor.css'; // the style of the component (.css not .scss!)
/**
 * Component:
 * 1. Config the state and props of the component in the respectives files in the same directory
 * 2. Pass to the component the type of state and props => React.Component<Props, State>
 * 3. Ref constructor
 */
export default class DefaultCodeEditor extends React.Component<Props, State> {
    /**
     * Constuctor
     * @param props Props of the component (ref './Props.tsx')
     */
    constructor(props: Props) {
        super(props);
        this.state = {
            code: `\n// Javascript \nconsole.log('Hello world!');`
        };
        this.updateCode = this.updateCode.bind(this);
    }
    /**
     * Default render methods:
     *  Use this to create your html template
     */
    render() {
        const options = {
            lineNumbers: true,
            mode: 'javascript',
            theme: 'monokai' // or dracula
        };
        return (
            <div className="default-code-editor">
                <CodeMirror
                    value={this.state.code}
                    onChange={this.updateCode}
                    options={options}
                />
            </div>
        );
    }

    updateCode(newCode: any): any {
        console.log(newCode);
        this.setState({
            code: newCode
        });
    }
}