/**
 * External dependencies
 */
import * as React from 'react';
/**
 * Locals dependencies
 */
import { Props } from './Props'; // the props of the component
import { State } from './State'; // the state of the component
import './DefaultComponent.css'; // the style of the component (.css not .scss!)
/**
 * Component:
 * 1. Config the state and props of the component in the respectives files in the same directory
 * 2. Pass to the component the type of state and props => React.Component<Props, State>
 * 3. Ref constructor
 */
export default class DefaultComponent extends React.Component<Props, State> {
    /**
     * Constuctor
     * @param props Props of the component (ref './Props.tsx')
     */
    constructor(props: Props) {
        super(props);
        // State of the component (ref './State.tsx')
        this.state = {
            msg: 'Hello',
            repeat: 3
        };
    }
    /**
     * Default render methods:
     *  Use this to create your html template
     */
    render() {
        /**
         * Init some stuff if needed
         */
        let helloMessage: any;
        if (this.props.othernames) { // othernames props is provide? then
            helloMessage = this.props.othernames.forEach(name => { // for each names
                return(
                    // return a <div> with the state msg and the name of the other one in the loop
                    <div>{this.state.msg} {name}! </div>
                );
            });
        } else {
            // else return only a <div> with the state message and the name
            helloMessage = <div>{this.state.msg} {this.props.name}</div>;
        }
        /**
         * Render the component
         */
        return(
            <div className="default-component">{helloMessage}</div> // finaly return the message in the component
        );
    }
}