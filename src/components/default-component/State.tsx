/**
 * Interface State
 * Declare here all the state type of the component
 */
export interface State {
    msg: string;
    repeat: number;
}