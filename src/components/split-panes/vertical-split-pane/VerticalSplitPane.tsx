/**
 * External dependencies
 */
import * as React from 'react';
import * as SplitPane from 'react-split-pane';
/**
 * Locals dependencies
 */
import { Props } from './Props'; // the props of the component
import { State } from './State'; // the state of the component
import './VerticalSplitPane.css'; // the style of the component (.css not .scss!)
/**
 * Component:
 * 1. Config the state and props of the component in the respectives files in the same directory
 * 2. Pass to the component the type of state and props => React.Component<Props, State>
 * 3. Ref constructor
 */
export default class VerticalSplitPane extends React.Component<Props, State> {
    /**
     * Constuctor
     * @param props Props of the component (ref './Props.tsx')
     */
    constructor(props: Props) {
        super(props);
        // State of the component (ref './State.tsx')
        this.state = {
        };
    }
    /**
     * Default render methods:
     *  Use this to create your html template
     */
    render() {

        /**
         * Render the component
         */
        return(
            <div className="vertical-split-pane">
                <SplitPane split="vertical" defaultSize={300} minSize={300} maxSize={850} >
                    <div className="left">
                        {this.props.left}
                    </div>
                    <div className="right">
                        {this.props.right}
                    </div>
                </SplitPane>
            </div>
        );
    }
}