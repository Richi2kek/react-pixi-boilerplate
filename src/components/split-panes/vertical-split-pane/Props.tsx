/**
 * Interface Props
 * Declare here all the props type of the component
 */
export interface Props {
    left: any;
    right: any;
}
