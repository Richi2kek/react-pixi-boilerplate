/**
 * External dependencies
 */
import * as React from 'react';
/**
 * Locals dependencies
 */
import VerticalSplitPane from '../vertical-split-pane/VerticalSplitPane';
import HorizontalSplitPane from '../horizontal-split-pane/HorizontalSplitPane';

import { Props } from './Props'; // the props of the component
import { State } from './State'; // the state of the component
import './TripleSplitPane.css'; // the style of the component (.css not .scss!)
/**
 * Component:
 * 1. Config the state and props of the component in the respectives files in the same directory
 * 2. Pass to the component the type of state and props => React.Component<Props, State>
 * 3. Ref constructor
 */
export default class TripleSplitPane extends React.Component<Props, State> {
    /**
     * Constuctor
     * @param props Props of the component (ref './Props.tsx')
     */
    constructor(props: Props) {
        super(props);
        // State of the component (ref './State.tsx')
        this.state = {
        };
    }
    /**
     * Default render methods:
     *  Use this to create your html template
     */
    render() {

        /**
         * Render the component
         */
        return (
            <div className="triple-split-pane">
                <VerticalSplitPane
                    left={this.props.first}
                    right={
                        <HorizontalSplitPane
                            top={this.props.second}
                            bottom={this.props.third}
                        />
                    }
                />
            </div>
        );
    }
}