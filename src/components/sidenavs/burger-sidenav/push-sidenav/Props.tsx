import { DefaultLink } from '../../../links/model/DefaultLink';
export interface Props {
    open: boolean;
    links: DefaultLink[];
}