import * as React from 'react';
import * as ReactBurgerMenu from 'react-burger-menu';

import { Props } from './Props';
import { State } from './State';
import './ScaleDownSidenav.css';
import { DefaultLinksList } from '../../../links/default-links-list/DefaultLinksList';

const ScaleDownMenu = ReactBurgerMenu.scaleDown;

export class ScaleDownSidenav extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }
    render() {
        return (
            <ScaleDownMenu
                right={true}
                customBurgerIcon={false}
                customCrossIcon={false}
                outerContainerId={'bubbleSidenav'}
                isOpen={this.props.open}
                pageWrapId={'page-wrap'}
                width={280}
            >
                <DefaultLinksList links={this.props.links} />
            </ScaleDownMenu>
        );
    }
}
