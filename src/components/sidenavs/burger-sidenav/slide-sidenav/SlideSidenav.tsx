import * as React from 'react';
import * as ReactBurgerMenu from 'react-burger-menu';

import { Props } from './Props';
import { State } from './State';
import './SlideSidenav.css';
import { DefaultLinksList } from '../../../links/default-links-list/DefaultLinksList';

const SlideMenu = ReactBurgerMenu.slide;

export class SlideSidenav extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }
    render() {
        return (
            <SlideMenu
                right={true}
                customBurgerIcon={false}
                customCrossIcon={false}
                outerContainerId={'bubbleSidenav'}
                isOpen={this.props.open}
                pageWrapId={'page-wrap'}
                width={280}
            >
                <DefaultLinksList links={this.props.links} />
            </SlideMenu>
        );
    }
}
