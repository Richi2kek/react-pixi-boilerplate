import * as React from 'react';
import * as ReactBurgerMenu from 'react-burger-menu';

import { Props } from './Props';
import { State } from './State';
import './ScaleRotateSidenav.css';
import { DefaultLinksList } from '../../../links/default-links-list/DefaultLinksList';

const ScaleRotateMenu = ReactBurgerMenu.scaleRotate;

export class ScaleRotateSidenav extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }
    render() {
        return (
            <ScaleRotateMenu
                right={true}
                customBurgerIcon={false}
                customCrossIcon={false}
                outerContainerId={'bubbleSidenav'}
                isOpen={this.props.open}
                pageWrapId={'page-wrap'}
                width={280}
            >
                <DefaultLinksList links={this.props.links} />
            </ScaleRotateMenu>
        );
    }
}
