import * as React from 'react';
import * as ReactBurgerMenu from 'react-burger-menu';

import { Props } from './Props';
import { State } from './State';
import './BubbleSidenav.css';

import { DefaultLinksList } from '../../../links/default-links-list/DefaultLinksList';

const BubbleMenu = ReactBurgerMenu.bubble;

export class BubbleSidenav extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

    }
    render() {
        
        return (
            <BubbleMenu
                right={true}
                customBurgerIcon={false}
                customCrossIcon={false}
                outerContainerId={'bubbleSidenav'}
                isOpen={this.props.open}
                pageWrapId={'page-wrap'}
                width={280}
            >
                <DefaultLinksList links={this.props.links} />
            </BubbleMenu>
        );
    }
}
