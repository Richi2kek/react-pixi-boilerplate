import * as React from 'react';

import { IDefaultLink } from '../../links/model/IDefaultLink';

export interface Props {
    children: React.ReactNode;
    links: IDefaultLink[];
    isOpen: boolean;
    animation?: string;
}