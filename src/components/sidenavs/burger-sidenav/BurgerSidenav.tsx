import * as React from 'react';

import { Props } from './Props';
import { State } from './State';

import './BurgerSidenav.css';

import { BubbleSidenav } from './bubble-sidenav/BubbleSidenav';
import { ElasticSidenav } from './elastic-sidenav/ElasticSidenav';
import { FallDownSidenav } from './fall-down-sidenav/FallDownSidenav';
import { PushRotateSidenav } from './push-rotate-sidenav/PushRotateSidenav';
import { PushSidenav } from './push-sidenav/PushSidenav';
import { ScaleDownSidenav } from './scale-down-sidenav/ScaleDownSidenav';
import { ScaleRotateSidenav } from './scale-rotate-sidenav/ScaleRotateSidenav';
import { SlideSidenav } from './slide-sidenav/SlideSidenav';
import { StackSidenav } from './stack-sidenav/StackSidenav';

export class BurgerSidenav extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        const menu = this.displayMenu();
        return (
            <div id="bubbleSidenav" className="bubble-sidenav">
                {menu}
                <main id="page-wrap">
                    {this.props.children}
                </main>
            </div>
        );
    }

    displayMenu() {
        const animation = this.props.animation;
        switch (animation) {
            case 'slide':
                return (
                    <SlideSidenav
                        open={this.props.isOpen}
                        links={this.props.links}
                    />
                );
            case 'stack':
                return (
                    <StackSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'elastic':
                return (
                    <ElasticSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'bubble':
                return (
                    <BubbleSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'push':
                return (
                    <PushSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'pushRotate':
                return (
                    <PushRotateSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'scaleDown':
                return (
                    <ScaleDownSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'scaleRotate':
                return (
                    <ScaleRotateSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            case 'fallDown':
                return (
                    <FallDownSidenav open={this.props.isOpen} links={this.props.links}/>
                );
            default:
                return (
                    <SlideSidenav open={this.props.isOpen} links={this.props.links}/>
                );
        }
    }
}
