import { DefaultLink } from '../model/DefaultLink';

export interface Props {
    link: DefaultLink;
}