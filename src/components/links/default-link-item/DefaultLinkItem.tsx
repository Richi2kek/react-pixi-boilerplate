import * as React from 'react';
import { Link } from 'react-router-dom';

import { State } from './State';
import { Props } from './Props';

export class DefaultLinkItem extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const link = this.props.link;
        return (
            <div className="default-link">
                <Link to={link.href}>
                    {link.name}
                </Link>
            </div>
        );
    }
}
