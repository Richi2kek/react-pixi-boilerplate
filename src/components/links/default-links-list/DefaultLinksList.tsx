import * as React from 'react';

import { State } from './State';
import { Props } from './Props';

import { DefaultLinkItem } from '../default-link-item/DefaultLinkItem';
import { IDefaultLink } from '../model/IDefaultLink';

import './DefaultLinksList.css';

export class DefaultLinksList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        const links = this.props.links;
        const linksList = links.map((link: IDefaultLink) => {
            return(
                <li key={link.name}>
                    <DefaultLinkItem link={link}/>
                </li>
            );
        });
        return (
            <div className="default-links-list">
                <ul>
                    {linksList}
                </ul>
            </div>
        );
    }
}
