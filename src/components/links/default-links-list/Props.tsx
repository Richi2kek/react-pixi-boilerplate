import { DefaultLink } from '../model/DefaultLink';

export interface Props {
    links: DefaultLink[];
}