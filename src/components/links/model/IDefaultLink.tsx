export interface IDefaultLink {
    name: string;
    href: string;
    logo?: any;
}