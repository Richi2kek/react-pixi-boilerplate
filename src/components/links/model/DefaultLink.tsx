import { IDefaultLink } from './IDefaultLink';

export class DefaultLink implements IDefaultLink {
    name: string;
    href: string;
    logo?: any;
    constructor(name: string, href: string, logo?: any) {
        this.name = name;
        this.href = href;
        this.logo = logo;
    }
}