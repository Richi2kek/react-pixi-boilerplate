import * as React from 'react';

import { Link } from 'react-router-dom';

import { Layout, Menu, Icon } from 'antd';
const { Header, Footer, Sider, Content } = Layout;

import Props from './Props';
import State from './State';
import './DefaultLayout.css';

export default class DefaultLayout extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isCollapsed: true
        };
        this.handleHeaderMenuClick = this.handleHeaderMenuClick.bind(this);
        this.handleSiderClick = this.handleSiderClick.bind(this);
    }

    render() {
        return (
            <div className="layout">
                <Sider
                    style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0, top: 63 }}
                    breakpoint="lg"
                    collapsedWidth="0"
                    collapsed={this.state.isCollapsed}
                    onCollapse={(collapsed, type) => { console.log(collapsed, type); }}
                >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} onClick={this.handleSiderClick}>
                        <Menu.Item key="1" Link="">
                            <Link to="/dashboard">
                                <Icon type="dashboard"/>
                                <span className="nav-text">Dashboard</span>
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['dashboard']}
                            style={{ lineHeight: '64px' }}
                            onClick={this.handleHeaderMenuClick}
                        >
                                <Menu.Item key="1">
                                {
                                    this.state.isCollapsed
                                    ? <Icon type="menu-unfold" />
                                    : <Icon type="menu-fold" />
                                }
                                </Menu.Item>
                        </Menu>
                    </Header>
                    <Content style={{ padding: '0 50px' }}>
                        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                            {this.props.children}
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        React-pixi-boilerplate ©2017 Created by Richi2Kek
                    </Footer>
                </Layout>
            </div>
        );
    }

    private handleHeaderMenuClick(event: any) {
        this.setState((prevState, props) => {
            return {
                isCollapsed: !prevState.isCollapsed
            };
        });
    }
    private handleSiderClick(event: any) {
        console.log(event);
        this.setState((prevState, props) => {
            return {
                isCollapsed: !prevState.isCollapsed
            };
        });
    }
}