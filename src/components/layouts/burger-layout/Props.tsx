import { IDefaultLink } from '../../links/model/IDefaultLink';

export default interface Props {
    links: IDefaultLink[];
    animation?: string;
    children?: any;
}
