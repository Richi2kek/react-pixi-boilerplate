import * as React from 'react';
import { Layout } from 'antd';
const { Footer, Content, Header } = Layout;

import { BurgerSidenav } from '../../sidenavs/burger-sidenav/BurgerSidenav';
import { BurgerToggleButton } from '../../buttons/burger-toggle-button/BurgerToggleButton';

import Props from './Props';
import State from './State';
import './BurgerLayout.css';

export class BurgerLayout extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isOpen: false
        };
        this.handleToggleButtonClick = this.handleToggleButtonClick.bind(this);
    }
    render() {
        return (
            <div className="burger-layout">
                <Layout>
                    <Header style={{ padding: 0 }}>
                        <div>
                            <BurgerToggleButton
                                isOpen={this.state.isOpen}
                                onClick={this.handleToggleButtonClick}
                            />
                        </div>
                    </Header>
                    <BurgerSidenav
                        isOpen={this.state.isOpen}
                        links={this.props.links}
                        animation={this.props.animation ? this.props.animation : 'slide'}
                    >
                        <Content className="content" style={{ padding: '0 50px' }}>
                            <div style={{ background: '#fff', padding: 24, minHeight: '100%' }}>
                                {this.props.children}
                            </div>
                        </Content>
                    </BurgerSidenav>
                    <Footer style={{ textAlign: 'center' }}>
                        React-pixi-boilerplate ©2017 Created by Richi2Kek
                    </Footer>
                </Layout>
            </div>
        );
    }

    handleToggleButtonClick() {
        console.log('ok');
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
}