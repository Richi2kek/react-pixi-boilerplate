import * as React from 'react';
import { Layout } from 'antd';
const { Footer, Content, Header } = Layout;

import { BurgerSidenav } from '../../sidenavs/burger-sidenav/BurgerSidenav';
// import { DefaultLink } from '../../../models/default-link/DefaultLink';

import Props from './Props';
import State from './State';
import './GameLayout.css';

export default class GameLayout extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }
    render() {

        return (
            <div className="layout">
                <Layout>
                    <Header style={{ padding: 0 }}>
                        <div className="logo" />
                    </Header>
                    <BurgerSidenav
                        isOpen={true}
                        links={[]}
                        animation="slide">
                        <Content className="content" style={{ padding: '0 50px' }}>
                            <div style={{ background: '#fff', padding: 24, minHeight: '100%' }}>
                                {this.props.children}
                            </div>
                        </Content>
                    </BurgerSidenav>
                    <Footer style={{ textAlign: 'center' }}>
                        React-pixi-boilerplate ©2017 Created by Richi2Kek
                    </Footer>
                </Layout>
            </div>
        );
    }
}