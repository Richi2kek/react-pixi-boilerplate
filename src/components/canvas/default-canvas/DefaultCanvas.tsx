
import * as React from 'react';
import * as Pixi from 'pixi.js';

import Props from './Props';
import State from './State';

import './DefaultCanvas.css';

/**
 * TODO:
 * 1. Able to load 2d or 3d game
 * 3. Switch lib ? (pixi, three ....)
 * 2. Style this shit (responsive too)
 * 4. Refactor this shit
 */

export default class DefaultCanvas extends React.Component<Props, State> {
    // Attributs
    app: Pixi.Application;
    appOptions: Pixi.ApplicationOptions;
    rendererOptions: Pixi.RendererOptions;
    gameCanvas: HTMLDivElement;
    windowListener: any; // type?
    height: number;
    width: number;
    ratio: number;
    assetpath: string = 'favicon.ico';
    // Constructor
    constructor(props: Props) {
        super(props);
        this.state = {

        };
    }
    // Lifecycle
    componentDidMount() {
        this.initAppRenderer();
    }
    componentWillUnmount() {
        this.app.stop();
    }
    // Render
    render() {
        let component = this;
        return (
            <div className="default-canvas">
                <div ref={(thisDiv: HTMLDivElement) => { component.gameCanvas = thisDiv; }} />
            </div>
        );
    }
    // Methods
    private initAppRenderer(): void {
        // if gameCanvas el and is parent exist
        if (this.gameCanvas.parentElement) {
            this.app = new Pixi.Application();
            this.gameCanvas.appendChild(this.app.view);
            // this.app.renderer.autoResize = true;
            this.height = this.gameCanvas.parentElement.clientHeight;
            this.width = this.gameCanvas.parentElement.clientWidth;
            this.app.renderer.resize(
                this.width,
                this.height
            );
            this.app.start();
        }
        this.setup();
        this.initResizeEvent();
    }

    private setup() {
        if (this.props.game) {
            this.props.game.start(this.app);
        } else {
            Pixi.loader
                .add('spritesheet', '/assets/mc.json')
                .load((loader: any, resources: any) => {
                    this.startDefaultGame(loader, resources);
                });
        }
    }

    private initResizeEvent() {
        this.windowListener = window.addEventListener('resize', () => {
            if (this.gameCanvas.parentElement) {
                this.height = this.gameCanvas.parentElement.clientHeight;
                this.width = this.gameCanvas.parentElement.clientWidth;
                this.app.renderer.resize(this.width, this.height);
            }
        });
    }

    private startDefaultGame(loader: any, resources: any) {
        if (this.gameCanvas.parentElement) {
            const explosionTextures = [];
            let i;
            for (i = 0; i < 26; i++) {
                let texture = Pixi.Texture.fromFrame(
                    'Explosion_Sequence_A ' + (i + 1) + '.png'
                );
                explosionTextures.push(texture);
            }
            for (i = 0; i < 30; i++) {
                let explosion = new Pixi.extras.AnimatedSprite(explosionTextures);
                explosion.x = Math.random() * this.app.renderer.width;
                explosion.y = Math.random() * this.app.renderer.height;
                explosion.anchor.set(0.5);
                explosion.rotation = Math.random() * Math.PI;
                explosion.scale.set(0.75 + Math.random() * 0.5);
                explosion.gotoAndPlay(Math.random() * 27);
                this.app.stage.addChild(explosion);
            }
            const iconTexture = Pixi.Texture.fromImage(this.assetpath);
            const iconSprite = new Pixi.Sprite(iconTexture);
            const iconPosX = this.gameCanvas.parentElement.clientWidth / 2;
            const iconPosY = this.gameCanvas.parentElement.clientHeight / 2;
            iconSprite.anchor.x = 0.5;
            iconSprite.anchor.y = 0.5;
            iconSprite.position.x = iconPosX;
            iconSprite.position.y = iconPosY;
            iconSprite.scale.x = 2;
            iconSprite.scale.y = 2;
            this.app.stage.addChild(iconSprite);
            this.app.ticker.add((delta) => {
                iconSprite.rotation += 0.05 * delta;
            });
        }
    }
}